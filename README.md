# MyReactExampleProjectFrom2022Year
 My React Example Project from 2022 year<br />
 I've been tasked to make Anime and Voice Actors API and frontend client that contains CRUD for it in MongoDB and React.<br />
 I also made similar website with Book Library API year ago.<br />
 It is placed in folder "Project2021" <br />
 <br />
 <b>To view project<br /></b>
 Go to Project2022 folder<br />
1. Download both backend and frontend folders, and install/run MongoDB with connection parameters listed in index.js of backend<br />
2. Then install dependencies for frontend folder with commands "yarn install"/"npm install" while in it's directory<br />
3. Then install dependencies for backend folder with commands "yarn install"/"npm install" while in it's directory<br />
4. Then run page from frontend folder with commands "yarn start"/"npm start" while in it's directory<br />
5. Then run backend from backend folder with command "node index.js" while in it's directory<br />
6. Depending on which url you ran Backend server, change url's in client's ducks/.operations.js files. Standard parameters are:<br />
const dbConnData = {<br />
    host: process.env.MONGO_HOST || '127.0.0.1',<br />
    port: process.env.MONGO_PORT || 27017,<br />
    database: process.env.MONGO_DATABASE || 'animeApi'<br />
};<br />
7. Project from 2021 has similar readme in it's folder, "Project2021"<br />

 <br />
 Website Mainly Uses:<br />
 React,<br />
 MongoDB,<br />
 Docker(contains mongo), not necessary to work though<br />
 <br />
 "Frontend" Folder Uses React with dependencies used as listed below:<br />
 <br />
 "dependencies": {<br />
     "@testing-library/jest-dom": "^5.11.4",<br />
     "@testing-library/react": "^11.1.0",<br />
     "@testing-library/user-event": "^12.1.10",<br />
     "axios": "^0.24.0", <- data fetching from url library<br />
     "formik": "^2.2.9", <- organised form library<br />
     "react": "^17.0.2", <- react<br />
     "react-dom": "^17.0.2", <- react library for dom rendering <br />
     "react-redux": "^7.2.6", <- redux, organises logic<br />
     "react-router-dom": "^5.1.2", <- react library for routing with urls<br />
     "react-scripts": "4.0.3",<br />
     "redux": "^4.1.2", <- redux, organises logic<br />
     "redux-api-middleware": "^3.2.1", <- redux library for applying additional functions beetwen fetching data and updating state<br />
     "redux-logger": "^3.0.6", <- shows redux state changes<br />
     "redux-thunk": "^2.4.0", <- redux library for applying additional functions beetwen fetching data and updating state<br />
     "uuid": "^8.3.2", <- random id generation<br />
     "uuidv4": "^6.2.12", <- random id generation<br />
     "web-vitals": "^1.0.1", <- additional statistics<br />
     "yup": "^0.32.11" <- validation library, used in this case with formik<br />
   },<br />
 "Backend" Folder Uses MongoDB <br />
 <br />
 "dependencies": {<br />
     "cors": "^2.8.5", <- library used for headers<br />
     "dotenv": "^8.2.0", <- library for loading .env files<br />
     "express": "^4.17.1", <- library used for server listening<br />
     "fs": "0.0.1-security", <- library for reading filestreams<br />
     "mongoose": "^5.10.9" <- database library for interacting with MongoDB<br />
   }<br />
 <br />
# Additionaly, I used in my react projects from 2021
material-ui<br />
SCSS<br />




