import './MainPage.scss';
import React,{useState,useEffect} from 'react';
import {
  Link,
} from "react-router-dom";


export default function BookComponent(props) {
  // props include: passedbookinfo(passed book object), (showDeleteCheckers(passed boolean form main page, if true, show button to mark book for delete), 
  // updateListOfBooksToDelete(updates list of books that will be deleted on click of button))
  //console.log(props.books)
    return <div className="bookMainDiv">
      <div>
        {
          props.showDeleteCheckers===false&&
          <Link className="styledLink" to={{pathname:`/book_details/${props.passedbookinfo.id}`,state:{books:props.books}}} onClick={()=>{}}><div><b>Click For Detailed Display</b></div></Link>
        }
      </div>
      {
          props.showDeleteCheckers===true&&
        <div className="deleteButton">
          <input type="button" value="Mark Book For Delete" onClick={()=>
          {
            if(props.listOfBooksToDelete.includes(props.passedbookinfo)===false)
            {
              props.updateListOfBooksToDelete(props.listOfBooksToDelete.concat([props.passedbookinfo]))
              console.log("book marked for delete")

            }
            else
            {
              props.updateListOfBooksToDelete(props.listOfBooksToDelete.filter(book2=>book2!==props.passedbookinfo))
              console.log("book unmarked from delete")
            }

          }
          }></input>
          {
            props.listOfBooksToDelete.includes(props.passedbookinfo)===true&&
            <b>   Marked To Delete!</b>
          }
        </div>
        }
      {
      props.passedbookinfo.image_url!=null&&
      <img src={props.passedbookinfo.image_url} alt="" onError={(e)=>{e.target.src="https://www.publicdomainpictures.net/pictures/280000/velka/not-found-image-15383864787lu.jpg"}}/>
      }
      <div className="descriptionDiv">
      <input type="button" value="Add/Remove From Favourites?" onClick={()=>
          {
            props.favourites.includes(props.passedbookinfo)===false&&
            props.addToFavouritesHandle(props.passedbookinfo)
            props.favourites.includes(props.passedbookinfo)===true&&
            props.updateFavourites(props.favourites.filter(element=>element!==props.passedbookinfo))
          }
          }></input>
          {
            props.favourites.includes(props.passedbookinfo)===true&&
            <div>In Favourites?: Yes</div>
          }
          {
            props.favourites.includes(props.passedbookinfo)===false&&
            <div>In Favourites?: No</div>
          }
        <div><b>Title: </b></div>
        <div>{props.passedbookinfo.title}</div>
        <div><b>Author: </b></div>
        <div>{props.passedbookinfo.author}</div>
        <div><b>Genre: </b></div>
        <div>{props.passedbookinfo.genre}</div>
        <div><b>Release Date: </b></div>
        <div>{props.passedbookinfo.release_date.substring(0,10)}</div>
        <div><b>Description: </b></div>
        <div>{props.passedbookinfo.description}</div>
        
      </div>
    </div>
}