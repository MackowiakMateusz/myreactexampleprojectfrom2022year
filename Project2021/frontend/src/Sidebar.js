
import React,{useState,useEffect} from 'react';
import Form from './Form.js'

export default function Sidebar(props) {
    function sorting(){
        return(
          <div className="addAndEditBookForm">
            <div>
              <div>
                <input type="button" value="Sort By Alphabet" 
                onClick={()=>sortByTitleAlphabetically()}></input>
              </div>
              <div>
                <input type="button" value="Sort By Date" 
                onClick={()=>sortByDate()}></input>
              </div>
              <div>
                <input type="button" value="Sort By Book ID" 
                onClick={()=>sortByID()}></input>
              </div>
            </div>
          </div>
        )
      }
      const sortByID = () => {
        const sorted = [...props.books].sort((a, b) => {
          return a.id - b.id;
        });
        props.updateBooks(sorted);
        //alert(sorted[0].name);
      };
      const sortByTitleAlphabetically = () => {
        const sorted = [...props.books].sort((a, b) => {
          if(a.title.toLowerCase()<=b.title.toLowerCase())
          {
            return -1;
          }
          else
          {
            return 1;
          }
          
        });
        props.updateBooks(sorted);
        //alert(sorted[0].name);
      };
      const sortByDate=()=>
      {
        const sorted = [...props.books].sort(function(a,b){
          // Turn your strings into dates, and then subtract them
          // to get a value that is either negative, positive, or zero.
          return new Date(a.release_date) - new Date(b.release_date);
        });
        props.updateBooks(sorted);
      }
    return(<div className="leftSidebar">
        <div>
            (Sidebar)
        </div>
        <div>
          <input type="button" value="Add Book!" onClick={props.handleShowEditAddFormButton}></input>
        </div>
        {
        props.showAddEditForm===true&&
        <Form 
        updateAddedBookErrorMessage={props.updateAddedBookErrorMessage}
        addedBookErrorMessage={props.addedBookErrorMessage}
        deleteMode={props.deleteMode}
        updateDeleteMode={props.updateDeleteMode}
        handleDeleteOfBooks={props.handleDeleteOfBooks}
      handleShowDeleteCheckers={props.handleShowDeleteCheckers}
      showSortingOptions={props.showSortingOptions}
      handleShowSortingOptions={props.handleShowSortingOptions}
      handleShowEditAddFormButton={props.handleShowEditAddFormButton}
      showAddEditForm={props.showAddEditForm}
      books={props.books}
      updateBooks={props.updateBooks}
      favourites={props.favourites}
      showFavourites={props.showFavourites}
      updateFavourites={props.updateFavourites}
      updateShowFavourites={props.updateShowFavourites}
      fetchBooks={props.fetchBooks}
        onlyAdd={true}
        onlyEdit={false}
        />
        }
        <div>
          <input type="button" value="Start Deleting Books!" onClick={()=>{props.handleShowDeleteCheckers();
            if(props.deleteMode===false){props.updateDeleteMode(true)}else{props.updateDeleteMode(false)}}}></input>
        </div>
        {
          props.showDeleteCheckers&&
          <div className="addAndEditBookForm">
            <input type="button" value="Delete All Marked Books!" 
            onClick={()=>props.handleDeleteOfBooks()}></input>
          </div>
          
        }
        <div>
          <input type="button" value="Start Sorting!" onClick={props.handleShowSortingOptions}></input>
        </div>
        {
          props.showSortingOptions&&
          sorting()
        }
        <div>
          <input type="button" value="Show Or Hide Favourites!" onClick={()=>{if(props.showFavourites===false){props.updateShowFavourites(true)}else{props.updateShowFavourites(false)}}}></input>
        </div>
        {props.showFavourites===true&&<div>You are showing favourites!</div>}
      </div>
    )
}