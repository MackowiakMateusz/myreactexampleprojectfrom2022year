
import axios from 'axios'
import React,{useState,useEffect} from 'react';
import {
  Link, useLocation, useParams
} from "react-router-dom";
import './BookDetails.scss';




export default function BookDetails()
{
  const {bookID} = useParams()
  const location = useLocation()
  const {books} = location.state
  const [book,updateBook]=useState({})
  
  useEffect(() => {
    const fetchBooks =async function(){
      return await (await axios.get('http://localhost:5000/api/book/'+bookID)).data
    }
    updateBook({})
    const source=axios.CancelToken.source()
    fetchBooks().then(data=>updateBook(data))
    source.cancel()
    console.log("Ksiazka Wczytana")
  }, [])
  function handleDelete()//handle delete from book details
  {
    setTimeout(() => {
      axios.delete("http://localhost:5000/api/book/"+bookID)
    }, 500);
    updateBook({})
    return console.log("usunieto ksiazke")
  }
  
  const [areYouSureToDelete,updateAreYouSureToDelete]=useState(false)
  // jeżeli false to nie pojawiają się opcje
  
  
  return (
    <div key={book.id} className="detailedBookMainDiv">
        <div className="detailedDescriptionDiv">
            {deletingBook()}
            <div className="styledLink">
              <Link className="styledLink" to={{pathname:`/book_details/${bookID}/editForm`,state:{passedBook:book,passedBooks:books}}} onClick={()=>{updateBook({})}}><div><b>Edit Book</b></div></Link>
            </div>
            <div>
                {
                book.image_url!=null&&
                <img src={book.image_url} alt="" onError={(e)=>{e.target.src="https://www.publicdomainpictures.net/pictures/280000/velka/not-found-image-15383864787lu.jpg"}}/>
                }
                <div><b>Title: </b></div>
                <div>{book.title}</div>
                <div><b>Author: </b></div>
                <div>{book.author}</div>
                <div><b>Genre: </b></div>
                <div>{book.genre}</div>
                <div><b>Release Date: </b></div>
                <div>{book.release_date!==undefined&&book.release_date.substring(0,10)}</div>
                <div><b>Description: </b></div>
                <div>{book.description}</div>
            </div>
        </div>
    </div>
  )
  function deletingBook()
  {
    return (<div><Link className="styledLink" to={`/`} onClick={()=>{updateBook({})}}><div><b>Back To Main Page</b></div></Link>
    <div className="styledLink">
    <input  type="button" value="Delete Book" onClick={()=>{if(areYouSureToDelete===false){updateAreYouSureToDelete(true)}else{updateAreYouSureToDelete(false)}}}></input>
    </div>
    {
      areYouSureToDelete===true&&
      <div >
        <div>
          Are you sure?
        </div>
        <div>
          <input className="styledLink"  type="button" value="No" onClick={()=>updateAreYouSureToDelete(false)}></input>
        </div>
        <div>
          
          <Link className="styledLink" to={`/`} onClick={()=>{}}><input  type="button" value="Yes" onClick={()=>handleDelete(book.id)}></input></Link>
        </div>
      </div>
    }
    </div>)
  }
}
  

/*
Title:
Nowy
Author:
2Nowy
Genre:
2Nowy
Release Date:
2000-02-01
Description:
Nowy
*/
