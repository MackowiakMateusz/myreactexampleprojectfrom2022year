import './MainPage.scss';
import axios from 'axios'
import React,{useState,useEffect} from 'react';


export default function Form(props){
  
  const [addedBookTitle,updateAddedBookTitle]=useState("")
  const [addedBookAuthor,updateAddedBookAuthor]=useState("")
  const [addedBookGenre,updateAddedBookGenre]=useState("")
  const [addedBookReleaseDate,updateAddedBookReleaseDate]=useState("")
  const [addedBookDescription,updateAddedBookDescription]=useState("")
  const [addedBookImageURL,updateAddedBookImageURL]=useState("")
  // copying data from book in case of editing it, to edit form, so user don;t need to remember data of edited book
  useEffect(()=>{
    if(props.addedBookTitle!==undefined)
    {
      updateAddedBookTitle(props.addedBookTitle)
    }
    if(props.addedBookAuthor!==undefined)
    {
      updateAddedBookAuthor(props.addedBookAuthor)
    }
    if(props.addedBookGenre!==undefined)
    {
      updateAddedBookGenre(props.addedBookGenre)
    }
    if(props.addedBookReleaseDate!==undefined)
    {
      updateAddedBookReleaseDate(props.addedBookReleaseDate)
    }
    if(props.addedBookDescription!==undefined)
    {
      updateAddedBookDescription(props.addedBookDescription)
    }
    if(props.addedBookImageURL!==undefined)
    {
      updateAddedBookImageURL(props.addedBookImageURL)
    }
  }, [])

  const addBookHandle=()=>
  {
    if(props.deleteMode===false)
    {
      let date = new Date(addedBookReleaseDate);
      date.setDate(date.getDate() + 1); 
    if(isNaN(Date.parse(addedBookReleaseDate))!==true&&addedBookTitle!==""&&props.deleteMode===false
    &&addedBookReleaseDate.match(/([012]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/)&&
    props.books.filter(book=>book.title===addedBookTitle)[0]===undefined)
    {
      axios.post('http://localhost:5000/api/book',
      {
        "title":addedBookTitle,
        "author":addedBookAuthor,
        "genre":addedBookGenre,
        "release_date":date,
        "description":addedBookDescription,
        "image_url":addedBookImageURL//https://bigimg.taniaksiazka.pl/images/popups/FF5/9788378396321.jpg <- przykladowy
      }
      ).then((response) => {
        console.log(response.message);
      }, (error) => {
        console.log(error.message);
        console.log("Prawdopodobnie juz istnieje taka ksiazka w bazie danych.");
      });
      console.log("Post Dodajacy Ksiazke Zadziałał")
      props.updateAddedBookErrorMessage("Wait..")
      setTimeout(() => {
        props.fetchBooks(1).then(data=>props.updateBooks(data))
      }, 500);
      props.updateAddedBookErrorMessage("Book Correctly Added!")
      //czyszczenie formularza tak jak mialo byc
    updateAddedBookTitle("")
    updateAddedBookAuthor("")
    updateAddedBookGenre("")
    updateAddedBookDescription("")
    updateAddedBookReleaseDate("")
    updateAddedBookImageURL("")
    }
    else
    {
      props.updateAddedBookErrorMessage("Bad Format Of Provided Release Date, Or Book With This Title Exists")
    }
    
    }
    else
    {
      props.updateAddedBookErrorMessage("Before Adding, Exit Delete Mode")
    }
    
  }
  const editBookHandle=()=>
  {
    let date = new Date(addedBookReleaseDate);
    date.setDate(date.getDate() + 1);
    if(isNaN(Date.parse(addedBookReleaseDate))!==true&&addedBookTitle!==""&&props.book.id!==undefined
    &&props.books.filter(element=>element.id===props.book.id)[0]!==undefined
    &&addedBookReleaseDate.match(/([012]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/))
    {
      axios.put("http://localhost:5000/api/book/"+props.book.id,
    {
      "title":addedBookTitle,
      "author":addedBookAuthor,
      "genre":addedBookGenre,
      "release_date":date,
      "description":addedBookDescription,
      "image_url":addedBookImageURL
    }
    ).then((response) => {
      console.log(response.message);
    }, (error) => {
      console.log(error.message);
    });
    console.log("Post Edytujacy Istniejaca Ksiazke Zadziałał")
    props.updateEditedBookErrorMessage("Wait..")
    
    props.updateEditedBookErrorMessage("Book Correctly Edited!")
    //czyszczenie formularza tak jak mialo byc
    updateAddedBookTitle("")
    updateAddedBookAuthor("")
    updateAddedBookGenre("")
    updateAddedBookDescription("")
    updateAddedBookReleaseDate("")
    updateAddedBookImageURL("")
    }
    else
    {
      props.updateEditedBookErrorMessage("Bad Format Of Provided Release Date, Or No Title Provided")
    }

    
  }

    return (<div className="addAndEditBookForm">
            <div className="addAndEditBookFormDiv">
              <div>
                {
                  props.onlyAdd===true&&
                  <b>Title Of Book To Add: </b>
                }
                {
                  props.onlyEdit===true&&
                  <b>Title Of Book To Edit: </b>
                }
              </div>
              <div>
                <input placeholder="Write Book Title" className="input" onFocus={(e) => e.target.placeholder = ''} onBlur={(e) => e.target.placeholder = "Write Book Title"}
                type="text" value={addedBookTitle} onChange={(event)=>updateAddedBookTitle(event.target.value)} />
              </div>
            </div>
            <div className="addAndEditBookFormDiv">
              <div>
                <b>Book Author: </b>
              </div>
              <div>
                <input placeholder="Write Book Author" className="input" onFocus={(e) => e.target.placeholder = ''} onBlur={(e) => e.target.placeholder = "Write Book Author"}
                type="text" value={addedBookAuthor} onChange={(event)=>updateAddedBookAuthor(event.target.value)} />
              </div>
            </div>
            <div className="addAndEditBookFormDiv">
              <div>
                <b>Book Genre: </b>
              </div>
              <div>
                <input placeholder="Write Book Genre" className="input" onFocus={(e) => e.target.placeholder = ''} onBlur={(e) => e.target.placeholder = "Write Book Genre"}
                type="text" value={addedBookGenre} onChange={(event)=>updateAddedBookGenre(event.target.value)} />
              </div>
            </div>
            <div className="addAndEditBookFormDiv">
              <div>
                <b>(YYYY-MM-DD)Release Date: </b>
              </div>
              <div>
                <input placeholder="YYYY-MM-DD" className="input" onFocus={(e) => e.target.placeholder = ''} onBlur={(e) => e.target.placeholder = "YYYY-MM-DD"}
                type="text" value={addedBookReleaseDate} onChange={(event)=>updateAddedBookReleaseDate(event.target.value)} />
              </div>
            </div>
            <div className="addAndEditBookFormDiv">
              <div>
                <b>Description: </b>
              </div>
              <div>
                <input placeholder="Write Book Description" className="input" onFocus={(e) => e.target.placeholder = ''} onBlur={(e) => e.target.placeholder = "Write Book Description"}
                type="text" value={addedBookDescription} onChange={(event)=>updateAddedBookDescription(event.target.value)} />
              </div>
            </div>
            <div className="addAndEditBookFormDiv">
              <div>
                <b>Image URL: </b>
              </div>
              <div>
                <input placeholder="https://..." className="input" onFocus={(e) => e.target.placeholder = ''} onBlur={(e) => e.target.placeholder = "https://..."}
                type="text" value={addedBookImageURL} onChange={(event)=>updateAddedBookImageURL(event.target.value)} />
              </div>
            </div>
            <div>
              {
                props.onlyAdd===true&&
                <input type="button" value="Add Your Book!" onClick={addBookHandle}></input>
              }
              {
                props.onlyEdit===true&&
                <input type="button" value="Edit Your Book!" onClick={editBookHandle}></input>
              }
              <div><b>{props.addedBookErrorMessage}</b></div>
            </div>
          </div>
    )
}