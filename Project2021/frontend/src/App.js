
import './MainPage.scss';
import React from 'react';
import {
  Switch,
  Route
} from "react-router-dom";
import { BrowserRouter as Router } from 'react-router-dom'
import MainPage from "./MainPage.js";
import BookDetails from "./BookDetails.js";
import BookDetailsEdit from "./BookDetailsEdit.js"


function App()
{
    return(
        <Router>
            <div className="App">
            <Switch>
                <Route exact path="/" component={MainPage} />
                <Route exact path="/book_details/:bookID">
                    <BookDetails/>
                </Route>
                <Route exact path="/book_details/:bookID/editForm">
                    <BookDetailsEdit/>
                </Route>
            </Switch>
            </div>
        </Router>
    )
}
export default App;