

import React,{useState,useEffect} from 'react';
import {
  Link, useLocation, useParams
} from "react-router-dom";
import Form from './Form.js';
import './BookDetails.scss';

export default function BookDetailsEdit()
{
  const {bookID} = useParams()
  
  const location = useLocation()
  const {passedBook, passedBooks} = location.state
  const [editedBookErrorMessage,updateEditedBookErrorMessage]=useState("")
  return(
    <div>
      <Link className="styledLink" to={`/`} onClick={()=>{}}><div><b>Back To Main Page</b></div></Link>
      {
        passedBook!==undefined&&
        <Form
        editedBookErrorMessage={editedBookErrorMessage}
        updateEditedBookErrorMessage={updateEditedBookErrorMessage}
        books={passedBooks}
        book={passedBook}
        addedBookTitle={passedBook.title}
        addedBookAuthor={passedBook.author}
        addedBookGenre={passedBook.genre}
        addedBookReleaseDate={passedBook.release_date.substring(0,10)}
        addedBookDescription={passedBook.description}
        addedBookImageURL={passedBook.image_url}
        onlyEdit={true}
        />
      }
      {
        <div><h1 className="errormessage">{editedBookErrorMessage}</h1></div>
      }
      
    </div>
    
  )
}