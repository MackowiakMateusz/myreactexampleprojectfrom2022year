import './MainPage.scss';
import axios from 'axios'
import React,{useState,useEffect} from 'react';
import BookComponent from './BookComponent.js';
import Sidebar from './Sidebar.js';
import Header from './Header.js';



export default function MainPage() {
  // variable that will hold database of books on each refresh
  const [books,updateBooks]=useState([])
  const [favourites,updateFavourites]=useState([])
  const addToFavouritesHandle=(book)=>{
    if(favourites.includes(book)===false)
    {
      updateFavourites(favourites.concat([book]))
    }
  }
  useEffect(() => {
    updateBooks([])
    updateFavourites([])
    const source=axios.CancelToken.source()
    setTimeout(() => {
      fetchBooks(1).then(data=>updateBooks(data))
    }, 1500);
    source.cancel()
  }, [])
  useEffect(() => {
    console.log("Dobra tutaj są wszystkie ksiązki, które są w bazie obecnie.")
    console.log(books)
  }, [books])
  
  async function fetchBooks(pagenumber){
    
    let responsesDatas=[]
    for (let i = 1; i <= pagenumber; i++) {
      responsesDatas.push((await axios.get('http://localhost:5000/api/book')));
    }
    let sumOfResponsesDatas={data:[]}
    for (let i = 0; i <= pagenumber-1; i++) {
      sumOfResponsesDatas.data=sumOfResponsesDatas.data.concat(responsesDatas[i])
    }
    
    return sumOfResponsesDatas.data[0].data  
  }

  const [showAddEditForm,updateShowAddEditForm]=useState(false)
  const [deleteMode,updateDeleteMode]=useState(false)

  
  const [showFavourites,updateShowFavourites]=useState(false)

  
  const [addedBookErrorMessage,updateAddedBookErrorMessage]=useState("")

  const [showDeleteCheckers,updateshowDeleteCheckers]=useState(false)
  const [listOfBooksToDelete,updateListOfBooksToDelete]=useState([])

  //3. Możliwość sortowania
  const[showSortingOptions,updateShowSortingOptions]=useState(false)
  //Alfabetycznie, wg. daty oraz wg. danych liczbowych [DST]
  // kolejnosc wg ktorej jest sortowane zalezy od tego ktory guzik pierwszy uizytkownik kliknie(jak odznaczy 3 to moze sobie znowu wybrac kolejnosc)
  

  
  
  const handleShowEditAddFormButton=()=>{if(showAddEditForm===true){updateShowAddEditForm(false)}else{updateShowAddEditForm(true)}}
  const handleShowDeleteCheckers=()=>{if(showDeleteCheckers===true){updateshowDeleteCheckers(false)}else{updateshowDeleteCheckers(true)}}
  const handleShowSortingOptions=()=>{if(showSortingOptions===true){updateShowSortingOptions(false)}else{updateShowSortingOptions(true)}}
const handleDeleteOfBooks=()=>{
  listOfBooksToDelete.forEach(bookToDelete=>{if(favourites.includes(bookToDelete)===true){updateFavourites(favourites.filter(book=>bookToDelete!==book))}})
  listOfBooksToDelete.forEach(book=>{axios.delete("http://localhost:5000/api/book/"+book.id)});
  updateListOfBooksToDelete([])
  setTimeout(() => {
    fetchBooks(1).then(data=>updateBooks(data))
  }, 500);
  updateshowDeleteCheckers(false)
  updateDeleteMode(false)
  }
  function bookRenderer(book)
  {
    return(<BookComponent 
      favourites={favourites}
      updateFavourites={updateFavourites}
      addToFavouritesHandle={addToFavouritesHandle}
      books={books}
      passedbookinfo={book} 
      showDeleteCheckers={showDeleteCheckers} 
      updateListOfBooksToDelete={updateListOfBooksToDelete}
      listOfBooksToDelete={listOfBooksToDelete}
      />
  )
  }
  // Tutaj zwracam MainPage
  return (
    <div className="mainDiv">
      <Header/>
      <Sidebar
      addedBookErrorMessage={addedBookErrorMessage}
      updateAddedBookErrorMessage={updateAddedBookErrorMessage}
      onlyAdd={true}
      onlyEdit={false}
      showDeleteCheckers={showDeleteCheckers}
      deleteMode={deleteMode}
      updateDeleteMode={updateDeleteMode}
      handleDeleteOfBooks={handleDeleteOfBooks}
      handleShowDeleteCheckers={handleShowDeleteCheckers}
      showSortingOptions={showSortingOptions}
      handleShowSortingOptions={handleShowSortingOptions}
      handleShowEditAddFormButton={handleShowEditAddFormButton}
      showAddEditForm={showAddEditForm}
      books={books}
      updateBooks={updateBooks}
      favourites={favourites}
      showFavourites={showFavourites}
      updateFavourites={updateFavourites}
      updateShowFavourites={updateShowFavourites}
      fetchBooks={fetchBooks}
      />
      <div className="booksContainer">
        <div  className="bookFlexContainer">
        {
          books!==undefined&&showFavourites===false&&

          books.map(book=><div key={book.id}>{bookRenderer(book)}</div>)
        }
        {
          books!==undefined&&showFavourites===true&&
          favourites.map(book=><div key={book.id}>{bookRenderer(book)}</div>)
        }
        </div>
      </div>
    </div>
  );
}


/*
books.filter(book=>book.title===editedBookTitle)[0].id
*/

/*
<div>Favourite?: {favourites!==undefined&&favourites.includes(book)===true&& <b>Yes</b>}{favourites!==undefined&&favourites.includes(book)===false&& <b>No</b>}</div>
        <div><input type="button" value="Add/Remove?(Favourites)" onClick={()=>{favourites.includes(book)===false&&updateFavourites(favourites.concat([book]));
        favourites.includes(book)===true&&updateFavourites(favourites.filter(element=>element!==book))}}></input></div>
*/